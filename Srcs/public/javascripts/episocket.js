$(document).ready(function() {
  var socket_addr = "dashboard.epi-hash.us";
  var socket = io.connect('https://' + socket_addr);
  var user_html = "<span class='lnr lnr-user'></span>";
  var cl = "\\l";

  socket.on('connect', function () {
    $('#chat').addClass('connected');
  });

  socket.on('announcement', function (msg) {
    $('#lines').append($('<p>').append($('<em>').text(msg)));
  });

  socket.on('nicknames', function (nicknames) {
    var uniques = [];
    $.each(nicknames, function(i, el){
      if($.inArray(el, nicknames) === -1) uniques.push(el);
    });
    for (var i in uniques) {
      $('#nicknames').append($('<span class="badge badge-light"> <b>').text(uniques[i]), '</span>');
    }
  });

  socket.on('user message', message);
  socket.on('reconnect', function () {
    $('#lines').remove();
    message('System', 'Reconnected to the server');
  });

  socket.on('reconnecting', function () {
    $('#lines').html('<p class="alert alert-secondary">' +
    'Attempting to re-connect to the server' + '</p>');
  });

  socket.on('error', function (e) {
    message('System', e ? e : 'A unknown error occurred');
  });

  function message (from, msg) {
    $('#lines').append($('<p class="alert alert-secondary">').append($('<b>').text(from + ' : "'), msg + '"'));
  }

  function checkfield(field) {
    var msg = field.trim();
    if (msg[0] != '\\' && (msg.length > 0 || msg.length < 350))  {
      return msg;
    } else if (msg.localeCompare(cl)) {
      $('#lines').html('');
      $('#message').val('').focus();
    }
    return null;
  }
  // dom manipulation
  $(function () {
    $('#set-nickname').submit(function (ev) {
      socket.emit('nickname', $('#nick').val(), function (set) {
        if (!set) {
          clear();
          return $('#chat').addClass('nickname-set');
        }
      });
      return false;
    });

    $('#send-message').submit(function () {
      var msg = checkfield($('#message').val());

      if (msg) {
        message('me', msg);
        socket.emit('user message', $('#message').val());
        clear();
        return false;
      }
      return false;
    });

    function clear () {
      $('#message').val('').focus();
    }
  });

});
