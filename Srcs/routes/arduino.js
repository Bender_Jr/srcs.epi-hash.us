var express = require('express');
var router = express.Router();

var Pumps_Pins = [{name: "CTRL_Pompe_Droite", wires: [ {blanc: "signal"} ]}];

var tagline = "Any code of your own that you haven't looked at \
for six or more months might as well have been written by someone else.";
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('pages/arduino',
  {
    title: ' Arduino WebCTrl',
    Pins: Pumps_Pins,
    tagline: tagline
  });
});

module.exports = router;
